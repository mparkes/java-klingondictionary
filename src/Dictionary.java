
public class Dictionary {

	private String dictionary[][];
	private int size = 2;
	private int index;
	
	private int englishIndex = 0;
	private int klingonIndex = 1;
	
	public Dictionary(){
		dictionary = new String[2][size];
		index = -1;
	}
	
	/**
	 * Adds a word to the dictionary
	 * 
	 * @param english word
	 * @param klingon translation
	 */
	public void addWord(String english, String klingon){
		if(nextIndex()==-1)
			resizeDictionary();
		dictionary[englishIndex][index] = english;
		dictionary[klingonIndex][index] = klingon;
	}
	public String englishToKlingon(String english){
		for(int i=0; i<dictionary[0].length; i++){
			if(english.equalsIgnoreCase(dictionary[englishIndex][i]))
				return dictionary[klingonIndex][i];
		}
		return null;
	}
	public String klingonToEnglish(String klingon){
		for(int i=0; i<dictionary[0].length; i++){
			if(klingon.equals(dictionary[klingonIndex][i]))
				return dictionary[englishIndex][i];
		}
		return null;
	}
	/**
	 * Gets the number of words stored in dictionary
	 * 
	 * @return number of words
	 */
	public int getSize(){
		int words = 0;
		for(int i=0; i<dictionary[englishIndex].length; i++){
			if(dictionary[englishIndex][i]!=null)
				words++;
		}
		return words;
	}
	/**
	 * resize the dictionary to make room for new word
	 */
	private void resizeDictionary(){
		size = size*2;
		
		String newDictionary[][] = new String[2][size];
		for(int i=0; i<dictionary[0].length; i++){
			newDictionary[englishIndex][i] = dictionary[englishIndex][i];
			newDictionary[klingonIndex][i] = dictionary[klingonIndex][i];
		}
		
		dictionary = newDictionary;
	}
	/**
	 * checks if we can add a new word, if we can't then returns -1 else returns the index of the next word
	 * @return
	 */
	private int nextIndex(){
		index++;
		return (index == size) ? -1 : index; 
	}
}
